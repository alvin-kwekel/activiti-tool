package com.essent.be;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.http.common.HttpMethods;
import org.apache.camel.http.common.HttpOperationFailedException;

public class RunningProcesses extends RouteBuilder {

  public void configure() {
    onException(HttpOperationFailedException.class).
      handled(true).
      log("Deleting process instance ${header.processId} failed");

    from("direct:start").
      streamCaching().
      setHeader(Exchange.HTTP_METHOD, constant(HttpMethods.GET)).
      to("https://devint02.nova.essent.be/essent-bpm-be/api/history/historic-process-instances?order=desc&sort=startTime&size=20&finished=false&processDefinitionKey=Handle-Guarantee&authUsername=essent&authPassword=test&authMethod=basic").
      choice().when().jsonpath(".id").
        split().jsonpath(".id").
          setHeader("processId", simple("${body}", String.class)).
          setHeader(Exchange.HTTP_METHOD, constant(HttpMethods.DELETE)).
          toD("https://devint02.nova.essent.be/essent-bpm-be/api/runtime/process-instances/${header.processId}?authUsername=essent&authPassword=test&authMethod=basic").
          log("Deleted process instance ${header.processId}").
        end().
        to("direct:start");

    from("timer://foo?repeatCount=1").
      to("direct:start");
  }
}
